//
//  XMLManager.swift
//  Recipe
//
//

import Foundation

public class XMLManager {
    
    private(set) var loading: Bool = false
    public var categories : Array<Category> = []

    func parse(completionClosure: (error: NSError?) -> ()) {
        loading = true
        
        if RemoteXMLUrl.characters.count > 0 {
            let request = NSURLRequest(URL: NSURL(string: RemoteXMLUrl)!)
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(request) {
                (data, response, error) in
                if error == nil {
                    let result = NSString(data: data!, encoding:
                        NSUTF8StringEncoding)!
                    
                    self.parseXML(result as String)
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.loading = false
                    completionClosure(error: error)
                }
            }
            task.resume()
        } else {
            if let filePath = NSBundle.mainBundle().pathForResource("recipes_settings", ofType:"xml") {
                let data = NSData(contentsOfFile:filePath)
                let string = NSString(data: data!, encoding: NSUTF8StringEncoding)

                self.parseXML(string as? String)
                dispatch_async(dispatch_get_main_queue()) {
                    self.loading = false
                    completionClosure(error: nil)
                }
            }
        }
    }

    func parseXML(string: String?) {
        let xml = SWXMLHash.parse(string!)

        if let categories  = xml["Settings"].element?.elements["Category"] {
            for category in categories {
                let cat = Category();
                cat.name = category.attributes["name"]!
                cat.icon = category.attributes["icon"]!
                self.categories.append(cat)

                if let recipes = category.elements["RecipeInfo"] {
                    for r in recipes {
                        let recipe = Recipe()
                        recipe.recipeId = Int(r.attributes["id"]!)!
                        recipe.name = r.elements["RecipeTitle"]![0].text!
                        cat.recipes.append(recipe)

                        var sum = r.elements["RecipeSummary"]![0].elements
                        let summary = RecipeSummary()
                        summary.origin = sum["RecipeSummaryOrigin"]![0].text!
                        summary.preparationTime = sum["RecipeSummaryPreparationTime"]![0].text!
                        summary.cookingTime = sum["RecipeSummaryCookingTime"]![0].text!
                        summary.portions = sum["RecipeSummaryPortions"]![0].text!
                        summary.calories = sum["RecipeSummaryCalories"]![0].text!
                        summary.desc = sum["RecipeSummaryDescription"]![0].text!
                        recipe.summary = summary

                        if let recipePictures = r.elements["RecipePictureList"]?[0].elements["RecipePicture"] {
                            for imageInfo in recipePictures {
                                if let iconTitle = imageInfo.text {
                                    recipe.icons.append(iconTitle)
                                }
                            }
                        }

                        for ingredientInfo in r.elements["RecipeIngredientsList"]![0].elements["RecipeIngredient"]! {
                            let ingredient = RecipeIngredient()
                            ingredient.name = ingredientInfo.elements["RecipeIngredientsName"]![0].text!
                            var quantity = ""
                            if let q = ingredientInfo.elements["RecipeIngredientsQuantity"]?[0].text {
                                quantity = q
                            }

                            ingredient.quantity = quantity
                            recipe.ingredients.append(ingredient)
                        }

                        for stepsInfo in r.elements["RecipeStepsList"]![0].elements["RecipeStep"]! {
                            let step = RecipeStep()
                            step.name = stepsInfo.elements["RecipeStepName"]![0].text!
                            step.desc = stepsInfo.elements["RecipeStepDescription"]![0].text!
                            recipe.steps.append(step)
                        }
                        
                        NSNotificationCenter.defaultCenter().postNotificationName("categoriesDownloaded", object: nil, userInfo: nil)
                    }
                }
                
            }
        }
    }
}