//
//  HomeCollectionViewLayout.swift
//  Recipe
//
//

import UIKit

class HomeCollectionViewLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        let currentItemAttributes = super.layoutAttributesForItemAtIndexPath(indexPath)
        let width = self.collectionView?.frame.width
        var frame = CGRectZero

        if let unwrappedWidth = width {
            switch indexPath.row {
            case 0:
                frame.size = CGSizeMake(unwrappedWidth, 200)
                break
            default:
                frame.size = CGSizeMake((unwrappedWidth/2) - 5, 120);
                break;
            }
        }

        currentItemAttributes!.frame = frame
        currentItemAttributes!.size = frame.size
        return currentItemAttributes
    }
}
