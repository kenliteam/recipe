//
//  ShoppingListViewController.swift
//  Recipe
//
//  Copyright (c) 2015 Codemotion. All rights reserved.
//

import UIKit

class ShoppingListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var ingredientsArray : Array<RecipeIngredient> = []

    override func viewDidLoad() {
        super.viewDidLoad()


        if let ingredients = NSUserDefaults.standardUserDefaults().objectForKey("shoppingList") as? Array<NSData> {

            ingredientsArray =  (NSKeyedUnarchiver.unarchiveObjectWithData(ingredients.first!) as? Array<RecipeIngredient>)!
        }

        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(image: UIImage(named: "nav_shopping_list_reset"), style: UIBarButtonItemStyle.Plain, target: self, action: "reset"), UIBarButtonItem(image: UIImage(named: "nav_shopping_list_clear"), style: UIBarButtonItemStyle.Plain, target: self, action: "clear")]
    }

    func reset() {
        for ingredient in ingredientsArray {
            ingredient.checked = false
        }

        tableView.reloadData()
    }

    func clear() {
        ingredientsArray = []

        NSUserDefaults.standardUserDefaults().setObject([NSKeyedArchiver.archivedDataWithRootObject(ingredientsArray)], forKey: "shoppingList")
        NSUserDefaults.standardUserDefaults().synchronize()

        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSUserDefaults.standardUserDefaults().setObject([NSKeyedArchiver.archivedDataWithRootObject(ingredientsArray)], forKey: "shoppingList")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    // MARK: - UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredientsArray.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let ingredient = ingredientsArray[indexPath.row]

        let text = "\(ingredient.quantity) \(ingredient.name)"
        cell.textLabel?.text = text
        var attr = NSAttributedString(string: text, attributes: [NSStrikethroughStyleAttributeName:NSUnderlineStyle.StyleNone.rawValue])
        if ingredient.checked {
            attr = NSAttributedString(string: text, attributes: [NSStrikethroughStyleAttributeName:NSUnderlineStyle.StyleSingle.rawValue])
        }

        cell.textLabel?.attributedText = attr

        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let ingredient = ingredientsArray[indexPath.row]
        ingredient.checked = !ingredient.checked
        
        tableView.reloadData()
    }
}
