//
//  RecipesViewController.swift
//  Recipe
//
//

import UIKit
import iAd

class RecipesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ADBannerViewDelegate {

    internal var recipes : Array<Recipe> = []
    var categoryName = ""
    var selectedRecipe = Recipe()
    private let RecipeListCellIdentifier = "RecipeListCellIdentifier"
    private let RecipeDetailsIdentifier = "RecipeDetailsIdentifier"
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!

    var adBannerView : ADBannerView = ADBannerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = categoryName

        if EnableAds {
            loadAds()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count;
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: RecipesListCell = tableView.dequeueReusableCellWithIdentifier(RecipeListCellIdentifier, forIndexPath: indexPath) as! RecipesListCell

        let recipe = recipes[indexPath.row];

        if recipe.icons.count > 0 {
            let icon = recipe.icons[0]
            if RemoteXMLUrl.characters.count > 0 {
                cell.recipeImageView.hnk_setImageFromURL(NSURL(string: icon)!)
            } else {
                cell.recipeImageView.image = UIImage(named: icon)
            }
        }


        cell.titleLabel.text = recipe.name;
        cell.countryLabel.text = recipe.summary.origin
        cell.preparationTimeLabel.text = recipe.summary.preparationTime
        cell.cookTimeLabel.text = recipe.summary.cookingTime
        cell.caloriesLabel.text = recipe.summary.calories

        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedRecipe = recipes[indexPath.row]

        self.performSegueWithIdentifier(RecipeDetailsIdentifier, sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc = segue.destinationViewController as! RecipeViewController
        vc.recipe = selectedRecipe
        vc.title = categoryName
    }

    func loadAds() {
        adBannerView = ADBannerView(frame: CGRect(x: 0, y: view.frame.height - 108, width: view.bounds.width, height: 44))
        adBannerView.delegate = self
        adBannerView.hidden = true
        view.addSubview(adBannerView)
    }

    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        collectionViewBottomConstraint.constant = 0
        banner.hidden = true
    }

    func bannerViewDidLoadAd(banner: ADBannerView!) {
        collectionViewBottomConstraint.constant = 44
        banner.hidden = false
    }
}
