//
//  RecipeViewController.swift
//  Recipe
//
//

import UIKit
import iAd

class RecipeViewController: UIViewController, UITableViewDelegate, ADBannerViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIAlertViewDelegate {

    var recipe = Recipe()

    var imageName = ""
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var summaryImageView: UIImageView!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var ingredientsImageView: UIImageView!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeLabel: UILabel!
    @IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!

    var adBannerView : ADBannerView = ADBannerView()

    var favorited = false;

    var selectedTab = 0;

    override func viewDidLoad() {
        super.viewDidLoad()

        title = nil
        let favorites = NSUserDefaults.standardUserDefaults().arrayForKey("favorites") as! Array<Int>?
        let recipeID = recipe.recipeId

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_add_shopping_list"), style: UIBarButtonItemStyle.Plain, target: self, action: "addToShoppingList:")
        
       self.tableView.estimatedRowHeight = 44.0
        
        imageName = "fav"
        if let f = favorites {
            if let i = f.indexOf(recipeID) {
                imageName = "fav_added"
            }
        }

        if EnableAds {
            loadAds()
        }
    }

    // MARK: - UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 1
        if selectedTab == 0 {
            rows += 1
        } else if selectedTab == 1 {
            rows += recipe.ingredients.count
        } else if selectedTab == 2 {
            rows += recipe.steps.count
        }

        return rows
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: RecipeMainCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! RecipeMainCell

//            if recipe.icons.count > 0 {
//                let icon = recipe.icons[0]
//                if RemoteXMLUrl.characters.count > 0 {
//                    cell.recipeImage.hnk_setImageFromURL(NSURL(string: icon)!)
//                } else {
//                    cell.recipeImage.image = UIImage(named: icon)
//                }
//            }

            cell.favoriteButton .setImage(UIImage(named: imageName), forState: .Normal)
            cell.titleLabel.text = recipe.name
            cell.countryLabel.text = recipe.summary.origin
            cell.prepLabel.text = recipe.summary.preparationTime
            cell.cookLabel.text = recipe.summary.cookingTime
            cell.mealLabel.text = recipe.summary.portions
            cell.caloriesLabel.text = recipe.summary.calories

            return cell
        }
        if selectedTab == 0 {
            let cell: RecipeSummaryCell = tableView.dequeueReusableCellWithIdentifier("SummaryCellIdentifier", forIndexPath: indexPath) as! RecipeSummaryCell

            cell.summaryLabel.text = recipe.summary.desc
            
            return cell

        } else if selectedTab == 1 {
            let cell: RecipeIngredientCell = tableView.dequeueReusableCellWithIdentifier("RecipeIngredientCellIdentifier", forIndexPath: indexPath) as! RecipeIngredientCell

            let ingredient = recipe.ingredients[indexPath.row - 1]
            cell.quantityLabel.text = ingredient.quantity
            cell.ingredientLabel.text = ingredient.name

            return cell
            
        } else if selectedTab == 2 {
            let cell: RecipeInstructionCell = tableView.dequeueReusableCellWithIdentifier("RecipeCellIdentifier", forIndexPath: indexPath) as! RecipeInstructionCell

            let step = recipe.steps[indexPath.row - 1]
            cell.titleLabel.text = step.name
            cell.bodyLabel.text = step.desc

            return cell
        }

        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("SummaryCellIdentifier", forIndexPath: indexPath) 

        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            return 280
        }

        return UITableViewAutomaticDimension
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // Set cell width to 100%
        return collectionView.frame.size
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipe.icons.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : RecipeCell = collectionView.dequeueReusableCellWithReuseIdentifier("RecipeCell", forIndexPath: indexPath) as! RecipeCell

        if recipe.icons.count > 0 {
            let icon = recipe.icons[indexPath.row]
            if RemoteXMLUrl.characters.count > 0 {
                cell.imageView.hnk_setImageFromURL(NSURL(string: icon)!)
            } else {
                cell.imageView.image = UIImage(named: icon)
            }
        }
        return cell
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

    }


    @IBAction func summaryButtonTapped(sender: AnyObject) {
        summaryImageView.image = UIImage(named: "tab-bar-summary_active");
        ingredientsImageView.image = UIImage(named: "tab-bar-ingredients");
        recipeImageView.image = UIImage(named: "tab-bar-recipe");

        selectedTab = 0

        summaryLabel.textColor = UIColor(red: 86/255, green: 165/255, blue: 92/255, alpha: 1.0)
        ingredientsLabel.textColor = UIColor(red: 146/255, green: 146/255, blue: 146/266, alpha: 1.0)
        recipeLabel.textColor = UIColor(red: 146/255, green: 146/255, blue: 146/266, alpha: 1.0)

        tableView.reloadData()
    }

    @IBAction func ingredientsButtonTapped(sender: AnyObject) {
        summaryImageView.image = UIImage(named: "tab-bar-summary");
        ingredientsImageView.image = UIImage(named: "tab-bar-ingredients_active");
        recipeImageView.image = UIImage(named: "tab-bar-recipe");

        selectedTab = 1

        summaryLabel.textColor = UIColor(red: 146/255, green: 146/255, blue: 146/266, alpha: 1.0)
        ingredientsLabel.textColor = UIColor(red: 86/255, green: 165/255, blue: 92/255, alpha: 1.0)
        recipeLabel.textColor = UIColor(red: 146/255, green: 146/255, blue: 146/266, alpha: 1.0)

        tableView.reloadData()
    }

    @IBAction func recipeButtonTapped(sender: AnyObject) {
        summaryImageView.image = UIImage(named: "tab-bar-summary");
        ingredientsImageView.image = UIImage(named: "tab-bar-ingredients");
        recipeImageView.image = UIImage(named: "tab-bar-recipe_active");
        selectedTab = 2

        summaryLabel.textColor = UIColor(red: 146/255, green: 146/255, blue: 146/266, alpha: 1.0)
        ingredientsLabel.textColor = UIColor(red: 146/255, green: 146/255, blue: 146/266, alpha: 1.0)
        recipeLabel.textColor = UIColor(red: 86/255, green: 165/255, blue: 92/255, alpha: 1.0)
        
        tableView.reloadData()
    }

    @IBAction func favoriteButtonTapped(sender: AnyObject) {
        let recipeID = recipe.recipeId

        var favorites = NSUserDefaults.standardUserDefaults().arrayForKey("favorites") as! Array<Int>?

        imageName = "fav_added"
        if let f = favorites {
            if let i = f.indexOf(recipeID) {
                imageName = "fav"
                favorites?.removeAtIndex(i)
            } else {
                favorites?.append(recipeID)
            }
        } else {
            favorites = [recipeID]
        }

        self.tableView.reloadData()

        NSUserDefaults.standardUserDefaults().setObject(favorites, forKey: "favorites")
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    @IBAction func shareRecipe(sender: AnyObject) {
        let activity = UIActivityViewController(activityItems: [recipe.name, AppStoreUrl], applicationActivities: nil)

        presentViewController(activity, animated: true, completion: nil)
    }

    func loadAds(){
        adBannerView = ADBannerView(frame: CGRect(x: 0, y: view.frame.height - 108, width: view.bounds.width, height: 44))
        adBannerView.delegate = self
        adBannerView.hidden = true
        view.addSubview(adBannerView)
    }

    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        collectionViewBottomConstraint.constant = 0
        banner.hidden = true
    }

    func bannerViewDidLoadAd(banner: ADBannerView!) {
        collectionViewBottomConstraint.constant = 44
        banner.hidden = false
    }


    @IBAction func addToShoppingList(sender: AnyObject) {
        UIAlertView(title: "Do you want to add the ingredients into the Shopping List?", message: "", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK").show()
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {

            if let ingredients = NSUserDefaults.standardUserDefaults().objectForKey("shoppingList") as? Array<NSData> {

                var ingredientsArray =  (NSKeyedUnarchiver.unarchiveObjectWithData(ingredients.first!) as? Array<RecipeIngredient>)!

                ingredientsArray = ingredientsArray + recipe.ingredients
                NSUserDefaults.standardUserDefaults().setObject([NSKeyedArchiver.archivedDataWithRootObject(ingredientsArray)], forKey: "shoppingList")
                NSUserDefaults.standardUserDefaults().synchronize()
            } else {
                NSUserDefaults.standardUserDefaults().setObject([NSKeyedArchiver.archivedDataWithRootObject(recipe.ingredients)], forKey: "shoppingList")
                NSUserDefaults.standardUserDefaults().synchronize()
                print("ingredients")
            }

            print(NSUserDefaults.standardUserDefaults().objectForKey("shoppingList"))
        }
    }
}
