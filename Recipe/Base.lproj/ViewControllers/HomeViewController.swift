//
//  HomeViewController.swift
//  Recipe
//
//

import UIKit
import iAd

let RECIPES_SEGUE_IDENTIFIER = "RecipesViewController"

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ADBannerViewDelegate, UIAlertViewDelegate {
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!
    
    var adBannerView : ADBannerView = ADBannerView()
    var categories : Array<Category> = []
    var recipes : Array<Recipe> = []
    var regions : Array<String> = []
    var selectedCategoryRecipes : Array<Recipe> = []
    var categoryName = ""
    var leftMenu = LeftMenuViewController()
    var regionsViewController = RegionViewController()
    var favorites : Array<Recipe> = []
    var parser = XMLManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Home", comment: "Home")
        self.setupCollectionView()
        
        loadXML()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "hideLeftMenu:", name: "DidSelectLeftMenu", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "hideRegions:", name: "DidSelectRegion", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadData", name: "categoriesDownloaded", object: nil)
        
        if EnableAds {
            loadAds()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        searchView.hidden = true
        
        if !categories.isEmpty {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                
                self.favorites = []
                self.recipes = []
                
                for (index, value) in self.categories.enumerate() {
                    self.recipes += value.recipes
                }
                
                let fav = NSUserDefaults.standardUserDefaults().arrayForKey("favorites") as! Array<Int>?
                
                if let f = fav {
                    for (index, value) in self.recipes.enumerate() {
                        if let i = f.indexOf(value.recipeId) {
                            self.favorites.append(value)
                        }
                    }
                }
                
                for (index, value) in self.recipes.enumerate() {
                    if (self.regions.indexOf(value.summary.origin) == nil) {
                        self.regions.append(value.summary.origin)
                    }
                }
            })
        } else if !parser.loading {
            loadXML()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        searchField.resignFirstResponder()
        searchField.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData() {
        self.categories = parser.categories
        collectionView.reloadData()
    }
    
    func loadXML() {
        // Categories setup
        parser.parse() { error in
            
            if error != nil {
                var errorMessage = error?.localizedDescription
                
                if errorMessage == nil {
                    errorMessage = "Unknown error."
                }
                
                if #available(iOS 8.0, *) {
                    self.showAlertControllerWithMessage(errorMessage!)
                } else {
                    self.showAlertViewWithMessage(errorMessage!)
                }
                
            } else {
                self.categories = self.parser.categories
                self.collectionView.reloadData()
            }
        }
    }
    
    @available(iOS 8.0, *)
    func showAlertControllerWithMessage(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        let dismissAction = UIAlertAction(title: "Close", style: .Cancel, handler: nil)
        let retryAction = UIAlertAction(title: "Retry", style: .Default) { action in
            self.presentedViewController?.dismissViewControllerAnimated(true, completion:  nil)
            self.loadXML()
        }
        
        alertController.addAction(dismissAction)
        alertController.addAction(retryAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showAlertViewWithMessage(message: String) {
        let alertView = UIAlertView(title: nil, message: message, delegate: self, cancelButtonTitle: "Close")
        alertView.addButtonWithTitle("Retry")
        alertView.show()
    }
    
    //MARK: - UIAlertViewDelegate
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if buttonIndex != alertView.cancelButtonIndex {
            loadXML()
        }
    }
    
    //MARK: - Collectoin View
    func setupCollectionView() {
        let flowLayout = HomeCollectionViewLayout()
        flowLayout.scrollDirection = UICollectionViewScrollDirection.Vertical
        flowLayout.minimumInteritemSpacing = 10.0
        flowLayout.minimumLineSpacing = 10.0
        
        collectionView.collectionViewLayout = flowLayout;
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return collectionViewLayout.layoutAttributesForItemAtIndexPath(indexPath)!.size
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : HomeCell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCell", forIndexPath: indexPath) as! HomeCell
        let category = categories[indexPath.row]
        print(category.icon)
        cell.bgImage?.image = UIImage(named: category.icon as String)
        cell.nameLabel?.text = "  "+(category.name as String)
        cell.recipesCoujntLabel.text = String(category.recipes.count)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let category = categories[indexPath.row]
        self.selectedCategoryRecipes = category.recipes
        categoryName = category.name as String
        self.performSegueWithIdentifier(RECIPES_SEGUE_IDENTIFIER, sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == RECIPES_SEGUE_IDENTIFIER {
            let vc = segue.destinationViewController as! RecipesViewController
            vc.recipes = self.selectedCategoryRecipes
            vc.categoryName = categoryName
        }
    }
    
    @IBAction func leftMenuTapped(sender: AnyObject) {
        
        let storyboard = self.storyboard!
        leftMenu = storyboard.instantiateViewControllerWithIdentifier("LeftMenuID") as! LeftMenuViewController
        leftMenu.categories = categories
        self.view.window!.addSubview(leftMenu.view)
    }
    
    @IBAction func searchButtonTapped(sender: AnyObject) {
        searchView.hidden = !searchView.hidden
    }
    
    func hideLeftMenu(notification: NSNotification) {
        leftMenu.view.removeFromSuperview()
        
        var info = notification.userInfo!
        let index = info["selectedRow"] as! Int
        
        if index >= 0 && index < categories.count {
            let category = categories[index]
            self.selectedCategoryRecipes = category.recipes
            categoryName = category.name as String
            self.performSegueWithIdentifier(RECIPES_SEGUE_IDENTIFIER, sender: self)
        } else if index == categories.count {
            self.selectedCategoryRecipes = favorites
            categoryName = "Favorites"
            self.performSegueWithIdentifier(RECIPES_SEGUE_IDENTIFIER, sender: self)
            
        } else if index == categories.count + 1 {
            self.performSegueWithIdentifier("ShoppingList", sender: self)
        } else if index == categories.count + 2 {
            self.performSegueWithIdentifier("AboutViewIdentifier2", sender: self)
        }
    }
    
    func hideRegions(notification: NSNotification) {
        regionsViewController.view.removeFromSuperview()
        
        if let info = notification.userInfo {
            let region = info["region"] as! String
            var searchRecipes : Array<Recipe> = []
            for (index, value) in recipes.enumerate() {
                if  value.summary.origin == region {
                    searchRecipes.append(value)
                }
            }
            
            self.selectedCategoryRecipes = searchRecipes
            categoryName = "Filter"
            self.performSegueWithIdentifier(RECIPES_SEGUE_IDENTIFIER, sender: self)
        }
    }
    
    @IBAction func search(sender: AnyObject) {
        var searchRecipes : Array<Recipe> = []
        for (index, value) in recipes.enumerate() {
            if  value.name.lowercaseString.rangeOfString(searchField.text!.lowercaseString) != nil {
                searchRecipes.append(value)
            }
        }
        
        self.selectedCategoryRecipes = searchRecipes
        categoryName = "Search"
        self.performSegueWithIdentifier(RECIPES_SEGUE_IDENTIFIER, sender: self)
    }
    
    @IBAction func filterButtonTapped(sender: AnyObject) {
        regionsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RegionViewControllerIdentifier") as! RegionViewController
        regionsViewController.regions = regions
        
        self.view.window!.addSubview(regionsViewController.view)
    }
    
    func loadAds(){
        adBannerView = ADBannerView(frame: CGRect(x: 0, y: view.frame.height - 108, width: view.bounds.width, height: 44))
        adBannerView.delegate = self
        adBannerView.hidden = true
        view.addSubview(adBannerView)
    }
    
    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        collectionViewBottomConstraint.constant = 0
        banner.hidden = true
    }
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        collectionViewBottomConstraint.constant = 44
        banner.hidden = false
    }
}
