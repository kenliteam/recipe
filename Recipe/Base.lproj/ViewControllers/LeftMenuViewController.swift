//
//  LeftMenuViewController.swift
//  Recipe
//
//

import UIKit

class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var categories : Array<Category> = []

    var images = ["left_main", "left_appet", "left_soup", "left_veg", "left_des", "left_timesaver", "left_other", "left_fav", "left_about"]
    var titles = ["Main Courses", "Appetizers", "Soups", "Vegetarian", "Deserts", "Time-Savers", "Other", "Favorites", "About Page"]
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: "tapOnBackground")
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tapOnBackground() {
        NSNotificationCenter.defaultCenter().postNotificationName("DidSelectLeftMenu", object: nil, userInfo: ["selectedRow" : -1])
    }

    // MARK: - UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count + 7
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: LeftMenuCell = tableView.dequeueReusableCellWithIdentifier("LeftMenuCellIdentifier", forIndexPath: indexPath) as! LeftMenuCell

        cell.menuImageView.image = nil
        cell.menuTitleLabel.text = nil
        if indexPath.row < categories.count {
            cell.menuImageView.image = UIImage(named: "left_"+(categories[indexPath.row].icon as String))
            cell.menuTitleLabel.text = categories[indexPath.row].name as String
        } else if indexPath.row == categories.count {
            cell.menuImageView.image = UIImage(named: "left_fav")
            cell.menuTitleLabel.text = "Favorites"
        } else if indexPath.row == categories.count + 1 {
            cell.menuImageView.image = UIImage(named: "left_menu_shopping_list")
            cell.menuTitleLabel.text = "Shopping List"
        } else if indexPath.row == categories.count + 2 {
            cell.menuImageView.image = UIImage(named: "left_about")
            cell.menuTitleLabel.text = "About Page"
        }

        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row > categories.count + 3 {
            return
        }
        NSNotificationCenter.defaultCenter().postNotificationName("DidSelectLeftMenu", object: nil, userInfo: ["selectedRow" : indexPath.row])
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        let point = touch.locationInView(self.view)
        if CGRectContainsPoint(tableView.frame, point) {
            return false
        }

        return true
    }
}
