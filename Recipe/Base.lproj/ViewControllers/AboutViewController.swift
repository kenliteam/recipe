//
//  AboutViewController.swift
//  Recipe
//
//

import UIKit
import iAd

class AboutViewController: UIViewController, ADBannerViewDelegate {
    var adBannerView : ADBannerView = ADBannerView()

    override func viewDidLoad() {
        super.viewDidLoad()

        if EnableAds {
            loadAds()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func facebookPressed(sender: UIButton) {
        let url = NSURL(string: FacebookUrl)
        UIApplication.sharedApplication().openURL(url!)
    }

    @IBAction func twitterButton(sender: UIButton) {
        let url = NSURL(string: TwitterUrl)
        UIApplication.sharedApplication().openURL(url!)
    }

    func loadAds(){
        adBannerView = ADBannerView(frame: CGRect(x: 0, y: view.frame.height - 108, width: view.bounds.width, height: 44))
        adBannerView.delegate = self
        adBannerView.hidden = true
        view.addSubview(adBannerView)
    }

    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        banner.hidden = true
    }

    func bannerViewDidLoadAd(banner: ADBannerView!) {
        banner.hidden = false
    }
}
