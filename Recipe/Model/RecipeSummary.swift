//
//  RecipeSummary.swift
//  Recipe
//
//

import UIKit

class RecipeSummary: NSObject {
    var origin = ""
    var preparationTime = ""
    var cookingTime = ""
    var portions = ""
    var calories = ""
    var desc = ""
}
