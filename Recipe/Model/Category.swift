//
//  Category.swift
//  Recipe
//
//

import UIKit

public class Category: NSObject {
    var name : NSString = ""
    var icon : NSString = ""
    var recipes : Array<Recipe> = []

    override public var description: String {
        return "name \(name) --- icon \(icon)"
    }
}
