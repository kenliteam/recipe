//
//  Recipe.swift
//  Recipe
//
//

import UIKit

public class Recipe: NSObject {
    var recipeId = 0
    var name = ""
    var icons = Array<String>()
    var summary = RecipeSummary()
    var ingredients = Array<RecipeIngredient>()
    var steps = Array<RecipeStep>()

    override public var description: String {
        return "name \(name) --- icons \(icons)"
    }
}
