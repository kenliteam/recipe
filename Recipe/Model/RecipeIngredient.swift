//
//  RecipeIngredient.swift
//  Recipe
//
//

import UIKit

class RecipeIngredient: NSObject {
    var name : String = ""
    var quantity : String = ""
    var checked : Bool = false

    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObjectForKey("name") as! String
        quantity = aDecoder.decodeObjectForKey("quantity") as! String
        checked = aDecoder.decodeBoolForKey("checked")
    }

    override init() {

        super.init()
    }

    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(quantity, forKey: "quantity")
        aCoder.encodeBool(checked, forKey: "checked")
    }

    override var description: String {
        return "name \(name) --- icon \(quantity)"
    }
}
