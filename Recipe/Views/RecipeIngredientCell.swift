//
//  RecipeIngredientCell.swift
//  Recipe
//
//

import UIKit

class RecipeIngredientCell: UITableViewCell {

    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var ingredientLabel: UILabel!
}
