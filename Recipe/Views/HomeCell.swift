//
//  HomeCell.swift
//  Recipe
//
//

import UIKit

class HomeCell: UICollectionViewCell {

    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var recipesCoujntLabel: UILabel!
    
}
