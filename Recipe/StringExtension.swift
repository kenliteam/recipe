//
//  UULabelExtension.swift
//  Recipe
//
//

import UIKit

extension NSString {

    func height(font: UIFont?, width: CGFloat ) -> CGFloat {
        let attributes = [NSFontAttributeName : font!]
        let rect = self.boundingRectWithSize(CGSizeMake(width, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes, context: nil)

        return rect.height
    }
}